#version 330 core

uniform vec4 color;
uniform sampler2D tex;

out vec4 FragColor;

in vec2 TexCoord;

void main() {
//	FragColor = color * texture(tex, TexCoord);
	FragColor = texture(tex, TexCoord);
}
