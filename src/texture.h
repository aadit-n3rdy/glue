#ifndef GLUE_TEXTURE_H
#define GLUE_TEXTURE_H

struct glue_texture {
	int width;
	int height;
	unsigned int tex_id;
};

int glue_load_texture (struct glue_texture *texture, const char *file);

#endif
