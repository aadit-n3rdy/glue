#ifndef GLUE_RENDERER_H
#define GLUE_RENDERER_H

#include "shape.h"
#include "color.h"
#include "window.h"
#include "sprite.h"

struct glue_renderer {
	unsigned int shape_shader;
	unsigned int sprite_shader;
	struct glue_window *window;
};

int glue_renderer_compile_shape_shader();

int glue_renderer_draw_shape(struct glue_renderer *renderer, struct glue_shape *shape);
int glue_renderer_draw_sprite(struct glue_renderer *renderer, struct glue_sprite *sprite);


#endif
