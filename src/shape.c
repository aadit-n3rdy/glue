#include "shape.h"
#include "vertex.h"

#include <GL/glew.h>

#include <stddef.h>
#include <stdlib.h>


int glue_shape_init(struct glue_shape *shape) {
	glGenBuffers(1, &shape->vbo);
	glGenVertexArrays(1, &shape->vao);
	glBindVertexArray(shape->vao);
	glBindBuffer(GL_ARRAY_BUFFER, shape->vbo);
	glBufferData(GL_ARRAY_BUFFER, 
			sizeof(struct glue_shape_vertex) * 
			shape->vertex_count,
			shape->vertices,
			GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(struct glue_shape_vertex), (void*)0);
	glEnableVertexAttribArray(0);
	return 0;
}

int glue_shape_ngon(struct glue_shape *shape,
		struct glue_shape_vertex *vertices,
		struct glue_rgba_n color,
		unsigned int vertex_count,
		unsigned int pos_x,
		unsigned int pos_y,
		float angle) {
	shape->pos_x = pos_x;
	shape->pos_y = pos_y;
	shape->angle = angle;
	shape->color = color;
	shape->scale_x = 1;
	shape->scale_y = 1;
	shape->vertex_count = vertex_count;
	shape->vertices = malloc(sizeof(struct glue_shape_vertex)*vertex_count);
	memcpy(shape->vertices, vertices, sizeof(struct glue_shape_vertex) * vertex_count);
	glue_shape_init(shape);
	return 0;
}

int glue_shape_free(struct glue_shape *shape) {
	shape->vertex_count = 0;
	free(shape->vertices);
	shape->vertices = NULL;
	return 0;
}

int glue_shape_rect(struct glue_shape *shape,
		float width,
		float height,
		struct glue_rgba_n color,
		unsigned int pos_x,
		unsigned int pos_y,
		float angle,
		unsigned char alignment) {

	struct glue_shape_vertex vertices[4];
	float offset_x = 0;
	float offset_y = 0;
	unsigned char hori, vert;
	vert = alignment & 3;
	hori = alignment & (3<<2);
	
	if (vert == GLUE_VERT_CENTRE) {
		offset_y = height/2;
	} else if (vert == GLUE_VERT_BOTTOM) {
		offset_y = height;
	}

	if (hori == GLUE_HORI_CENTRE) {
		offset_x = width/2;
	} else if (hori == GLUE_HORI_RIGHT) {
		offset_x = width;
	}

	vertices[0].pos_x = -offset_x;
	vertices[0].pos_y = -offset_y;
	vertices[1].pos_x = width - offset_x;
	vertices[1].pos_y = vertices[0].pos_y;
	vertices[2].pos_x = vertices[1].pos_x;
	vertices[2].pos_y = height - offset_y;
	vertices[3].pos_x = vertices[0].pos_x;
	vertices[3].pos_y = vertices[2].pos_y;

	//DEBUG
	printf("vert: %u hori: %u\n", vert, hori);
	printf("width: %f height: %f", width, height);
	int i = 0;

	for (i = 0; i < 4; i++) {
		printf("%d %f %f\n", i, vertices[0].pos_x, vertices[0].pos_y);
	}

	return glue_shape_ngon(shape, vertices, color, 4, pos_x, pos_y, angle);
}
