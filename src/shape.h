#ifndef GLUE_SHAPE_H
#define GLUE_SHAPE_H

#include "vertex.h"
#include "color.h"

#define GLUE_VERT_CENTRE 1
#define GLUE_VERT_TOP	 2
#define GLUE_VERT_BOTTOM 3

#define GLUE_HORI_CENTRE 1<<2
#define GLUE_HORI_LEFT	 2<<2
#define GLUE_HORI_RIGHT  3<<2


struct glue_shape {
	unsigned int pos_x;
	unsigned int pos_y;
	float angle;
	float scale_x;
	float scale_y;
	unsigned int vertex_count;
	struct glue_shape_vertex *vertices;
	struct glue_rgba_n color;
	unsigned int vbo;
	unsigned int vao;
};

int glue_shape_init(struct glue_shape *shape);

int glue_shape_ngon(struct glue_shape *shape,
		struct glue_shape_vertex *vertices,
		struct glue_rgba_n color,
		unsigned int vertex_count,
		unsigned int pos_x,
		unsigned int pos_y,
		float angle);

int glue_shape_free(struct glue_shape *shape);

int glue_shape_rect(struct glue_shape *shape,
		float width,
		float height,
		struct glue_rgba_n color,
		unsigned int pos_x,
		unsigned int pos_y,
		float angle,
		unsigned char alignment);

#endif
