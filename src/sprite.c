#include "color.h"
#include "sprite.h"

#include <GL/glew.h>

#include <stdlib.h>
#include <stdio.h>

int glue_sprite_alloc (struct glue_sprite *sprite) {
	glGenBuffers(1, &sprite->vbo);
	glGenVertexArrays(1, &sprite->vao);
	glBindVertexArray(sprite->vao);
	glBindBuffer(GL_ARRAY_BUFFER, sprite->vbo);
	glBufferData(GL_ARRAY_BUFFER,
			sizeof(sprite->vertices),
			sprite->vertices,
			GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(struct glue_sprite_vertex), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(struct glue_sprite_vertex), 
			(void*)(2*sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	float *data = malloc(sizeof(sprite->vertices));
	glGetBufferSubData(GL_ARRAY_BUFFER,
			0,
			sizeof(sprite->vertices),
			data);
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			printf("%f ", data[4*i + j]);
		}
		printf("\n");
	}
	return 0;
}

int glue_sprite_init (struct glue_sprite *sprite,
		unsigned int pos_x,
		unsigned int pos_y,
		float angle,
		unsigned int width,
		unsigned int height,
		struct glue_texture *tex,
		unsigned int tex_x,
		unsigned int tex_y,
		unsigned int tex_width,
		unsigned int tex_height) {

	sprite->pos_x = pos_x;
	sprite->pos_y = pos_y;
	sprite->angle = angle;
	sprite->scale_x = 1;
	sprite->scale_y = 1;
	sprite->vertices[0].pos_x = 0;
	sprite->vertices[0].pos_y = 0;
	sprite->vertices[1].pos_x = width;
	sprite->vertices[1].pos_y = 0;
	sprite->vertices[2].pos_x = width;
	sprite->vertices[2].pos_y = height;
	sprite->vertices[3].pos_x = 0;
	sprite->vertices[3].pos_y = height;
	sprite->texture = tex;
	sprite->animate_xoffset = 0;
	sprite->animate_yoffset = 0;
	sprite->tex_x = 0;
	sprite->tex_y = 0;
	sprite->frame_count = 0;
	sprite->cur_frame = 0;
	struct glue_rgba_n color;
	color.red = 1;
	color.green = 1;
	color.blue = 1;
	color.alpha = 1;
	sprite->color = color;

	if (tex != NULL) {
		if (tex_width == 0 || tex_height == 0) {
			tex_width = tex->width;
			tex_height = tex->height;
		}
		sprite->tex_width = tex_width;
		sprite->tex_height = tex_height;
		sprite->vertices[0].tex_x = tex_x;
		sprite->vertices[0].tex_y = tex_y;
		sprite->vertices[1].tex_x = tex_x + tex_width;
		sprite->vertices[1].tex_y = tex_y + 0;
		sprite->vertices[2].tex_x = tex_x + tex_width;
		sprite->vertices[2].tex_y = tex_y + tex_height;
		sprite->vertices[3].tex_x = tex_x + 0;
		sprite->vertices[3].tex_y = tex_y + tex_height;
	}

	glue_sprite_alloc(sprite);

	return 0;
}

int glue_sprite_reload_texcoords (struct glue_sprite *sprite) {
	glBindBuffer(GL_ARRAY_BUFFER, sprite->vbo);
	for (int i = 0; i < 4; i++) {
		glBufferSubData(GL_ARRAY_BUFFER,
				(void*)&sprite->vertices[i].tex_x - (void*)&sprite->vertices[0],
				2 * sizeof(float),
				&sprite->vertices[i].tex_x);
	}
}

int glue_sprite_set_texture (struct glue_sprite *sprite,
		struct glue_texture *tex,
		unsigned int tex_width,
		unsigned int tex_height) {

	sprite->texture = tex;
	if (tex_width == 0 || tex_height == 0) {
		tex_width = tex->width;
		tex_height = tex->height;
	}
	sprite->tex_width = tex_width;
	sprite->tex_height = tex_height;
	sprite->vertices[0].tex_x = 0;
	sprite->vertices[0].tex_y = 0;
	sprite->vertices[1].tex_x = tex_width;
	sprite->vertices[1].tex_y = 0;
	sprite->vertices[2].tex_x = tex_width;
	sprite->vertices[2].tex_y = tex_height;
	sprite->vertices[3].tex_x = 0;
	sprite->vertices[3].tex_y = tex_height;
	glue_sprite_reload_texcoords(sprite);
	
}



int glue_sprite_init_animate (struct glue_sprite *sprite,
		unsigned int xoffset,
		unsigned int yoffset,
		unsigned int frame_count) {
	sprite->animate_xoffset = xoffset;
	sprite->animate_yoffset = yoffset;
	sprite->frame_count = frame_count;
}

int glue_sprite_tick (struct glue_sprite *sprite) {
	int x_offset = sprite->animate_xoffset;
	int y_offset = 0;
	if (sprite->vertices[0].tex_x + x_offset > sprite->texture->width) {
		x_offset = sprite->tex_x - sprite->vertices[0].tex_x;
		y_offset = sprite->animate_yoffset;
	}
	if (sprite->cur_frame == sprite->frame_count) {
		sprite->cur_frame = 0;
		x_offset = sprite->tex_x - sprite->vertices[0].tex_x;
		y_offset = sprite->tex_y - sprite->vertices[0].tex_y;

	}
	for (int i = 0; i < 4; i++) {
		sprite->vertices[i].tex_x += x_offset;
		sprite->vertices[i].tex_y += y_offset;
	}
	glue_sprite_reload_texcoords (sprite);
	sprite->cur_frame++;
}

