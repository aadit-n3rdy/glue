#include "window.h"
#include "renderer.h"
#include "color.h"
#include "vertex.h"
#include "shape.h"
#include "input.h"
#include "texture.h"
#include "sprite.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

//#define STB_IMAGE_IMPLEMENTATION
//#include <stb_image.h>

#include <mosaic.h>

int main(void)
{

	printf("Hello World! \n");
	struct glue_window window;
	glue_init();
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);  
	glue_window_init(&window, 640, 480, "Hello World", 1);
	printf("Window inited\n");
	
//	struct glue_shape shape;
	struct glue_rgba_n color_fg_n = {1, 1, 1, 1};
//
//	glue_shape_rect(&shape, (float)50, (float)50, color_fg_n, 320, 240, 0, 
//			GLUE_HORI_CENTRE | GLUE_VERT_CENTRE);

	struct glue_renderer renderer;
	if (!glue_renderer_compile_shape_shader(&renderer)) {
		exit(0);
	}
	if (!glue_renderer_compile_sprite_shader(&renderer)) {
		exit(0);
	}

	renderer.window = &window;
	struct glue_rgba color = {0, 0, 0, 255};
	glue_renderer_set_bkg(color);

	float vertices[] = {
		0, 100, 0.0, 461,
		100, 100, 361, 461,
		100, 0, 361, 0.0,
		0, 0, 0.0, 0.0
	};

// WORKING BIT
//	unsigned int vao, vbo;
//	glGenBuffers(1, &vbo);
//	glGenVertexArrays(1, &vao);
//	glBindVertexArray(vao);
//	glBindBuffer(GL_ARRAY_BUFFER, vbo);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
//	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4, (void*)0);
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float)*4, (void*)(2*sizeof(float)));
//	glEnableVertexAttribArray(0);
//	glEnableVertexAttribArray(1);
//
//	float pos_x, pos_y, angle;
//	angle = 0;
//	pos_x = 100.0f;
//	pos_y = 100.0f;
// END OF WORKING BIT
//	int sw, sh, channels;
//	stbi_set_flip_vertically_on_load(GL_TRUE);
//	unsigned char *texdata = stbi_load("sprite.png", &sw, &sh, &channels, 4);
//	printf("Loaded texture %d %d \n", sw, sh);
//	unsigned int tex;
//	if (!texdata) {
//		printf("Could not load texdata\n");
//	}
//	glGenTextures(1, &tex);
//	glBindTexture(GL_TEXTURE_2D, tex);
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sw, sh, 0, GL_RGBA, GL_UNSIGNED_BYTE, texdata);
//	glGenerateMipmap(GL_TEXTURE_2D);
//	stbi_image_free(texdata);
//	printf("Created texture\n");

	struct glue_texture texture;
	glue_load_texture(&texture, "sprite.png");
	printf("tex1: %u\n", texture.tex_id);

	struct glue_texture tex2;
	glue_load_texture (&tex2, "spriteanim.png");
	printf("tex2: %u\n", tex2.tex_id);

	struct glue_sprite sprite;
	glue_sprite_init(&sprite, 100, 100, 0, 150, 191, &tex2, 0, 0, 150, 191);
	float counter = 0;
	glue_sprite_init_animate (&sprite, 150, 191, 8);

	while (!glfwWindowShouldClose(window.winptr) && 
			glue_get_keystate(&window, GLUE_KEY_ESCAPE)!= GLUE_PRESSED) {

		glue_renderer_clear();

		if (counter >= (float)5) {
			counter = 0;
			glue_sprite_tick(&sprite);
		} else {
			counter += 1;
		}

		glue_renderer_draw_sprite(&renderer, &sprite);

		glue_window_update(&window);

	}

	glue_terminate();
	return 0;
}
