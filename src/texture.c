
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "texture.h"

#include <GL/glew.h>

int glue_load_texture (struct glue_texture *texture, const char *file) {
	unsigned char *raw = stbi_load(file, &texture->width, &texture->height, NULL, 4);
	glGenTextures(1, &texture->tex_id);
	glBindTexture(GL_TEXTURE_2D, texture->tex_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width, texture->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, raw);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(raw);
	return 0;
}
