#ifndef GLUE_SPRITE_H
#define GLUE_SPRITE_H

#include "vertex.h"
#include "texture.h"
#include "color.h"

struct glue_sprite {
	unsigned int vao;
	unsigned int vbo;
	unsigned int pos_x;
	unsigned int pos_y;
	unsigned int tex_x;
	unsigned int tex_y;
	unsigned int tex_width;
	unsigned int tex_height;
	float angle;
	float scale_x;
	float scale_y;
	struct glue_sprite_vertex vertices[4];
	struct glue_texture *texture;
	struct glue_rgba_n color;
	unsigned int animate_xoffset;
	unsigned int animate_yoffset;
	unsigned int cur_frame;
	unsigned int frame_count;
};

int glue_sprite_init (struct glue_sprite *sprite,
		unsigned int pos_x,
		unsigned int pos_y,
		float angle,
		unsigned int width,
		unsigned int height,
		struct glue_texture *tex,
		unsigned int tex_x,
		unsigned int tex_y,
		unsigned int tex_width,
		unsigned int tex_height);

int glue_sprite_tick (struct glue_sprite *sprite);

int glue_sprite_init_animate (struct glue_sprite *sprite,
		unsigned int xoffset,
		unsigned int yoffset,
		unsigned int frame_count);

#endif
