
GLUE
====

This project is an OpenGL wrapper of sorts

Status: Under development

Contribution
-----------
> No hard and fast rules really
> Try and follow the kernel guidelines

Building from source
--------------------
Install glfw

Dependencies:
> glfw3

Credits
-------

OpenGameDev user bagzie https://opengameart.org/users/bagzie, for the bat sprite.
The GLFW project https://www.glfw.org/ for glfw.
The GLEW project https://github.com/nigels-com/glew
